﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Arrays_en_klassen
    {
        public static void StartSubmenu()
        {
            Console.Clear();
            Console.WriteLine("********************************");
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("********************************");
            Console.WriteLine("1. Prijzen met foreach (h11-prijzen)");
            Console.WriteLine("2. Speelkaarten (h11-speelkaarten)");
            Console.WriteLine("3. Organiseren van studenten (h11-organizer)");
          
            Console.Write("\rKeuze: ");

            int choice = int.Parse(Console.ReadLine());


            switch (choice)
            {
                case 1:
                    Console.Clear();
                    Prijzen.AskForPrices();
                    break;
                case 2:
                    Console.Clear();
                    PlayingCard.main();
                    break;
                case 3:
                    Console.Clear();
                    StudentProgram.Main();
                    break;
              
                default:
                    Console.Clear();
                    Console.WriteLine("Ongeldige keuze.");
                    break;
            }
        }
    }
}
