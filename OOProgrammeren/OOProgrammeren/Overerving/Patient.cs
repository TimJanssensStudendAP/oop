﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Patient
    {
        public string Name { get; set; }
        public uint DaysInHospital { get; set; }

        public Patient(string name, uint daysInHospital)
        {
            this.Name = name;
            this.DaysInHospital = daysInHospital;
        }

        public virtual void ShowCost()
        {
            Console.WriteLine($"{this.Name}, een gewone patiënt die {this.DaysInHospital} uur in het ziekenhuis gelegen heeft, betaalt €{this.CalculateTotalCost():F2}.");
        }
        public int CalculateTotalCost()
        {
            //bij 0 dagen moet er niet betaald worden
            if (DaysInHospital == 0)
                return 0;
            else
            {
                const int BasicRate = 50;
                const int HourRate = 20;
                return BasicRate + ((int)DaysInHospital * HourRate);
            }
        }

    }
}
