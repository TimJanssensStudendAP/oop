﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class InsuredPatient : Patient
    {

        public InsuredPatient(string name, uint daysInHospital) : base(name, daysInHospital)
        {

        }

        public override void ShowCost()
        {
            double cost = base.CalculateTotalCost() - ((double)base.CalculateTotalCost() * 0.1);
            Console.WriteLine($"{this.Name}, een verzekerde patiënt die {this.DaysInHospital} uur in het ziekenhuis gelegen heeft, betaalt €{cost:F2}.");

        }



    }
}
