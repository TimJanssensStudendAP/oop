﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Carexe
{
    class Car
    {

        private double odometer;

        public double Odometer
        {
            get { return odometer; }
            set { odometer = value; }
        }

        private double speed = 0;

        public double Speed
        {
            get { return speed; }
            set
            {
                if (Odometer == 0 || (Speed >= 10 && speed <= 110))
                {
                    speed = value;
                }
            }
        }

        public void Gas()
        {
            Speed += 10;

            Odometer += (((Speed - 10) + Speed) / 2) / 60;
        }

        public void Brake()
        {

            Speed -= 10;

            Odometer += (((Speed + 10) + Speed) / 2) / 60;

        }

        public void Gas(int times)
        {
            for (int i = 0; i < times; i++)
            {
               Gas();
            }
        }

        public void Brake(int times)
        {
            for (int i = 0; i < times; i++)
            {
                Brake();
            }
        }






    }
}
