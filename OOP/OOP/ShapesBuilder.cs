﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Geometry
{
    class ShapesBuilder
    {
        private ConsoleColor color = ConsoleColor.White;
        public ConsoleColor Color
        {
            get { return color; }
            set
            {
                color = value;
                Console.ForegroundColor = color;
            }
        }

        private char symbol = '*';

        public char Symbol
        {
            get { return symbol; }
            set { symbol = value; }
        }


        public string Line(int length)
        {
            return new string('-', length);
        }

        public string Line(int length, char alternateSymbol)
        {
            return new string(alternateSymbol, length);
        }

        public string Rectangle(int height, int width)
        {
            return Rectangle(height, width, Symbol, Color);
        }

        public string Rectangle(int height, int width, char alternateSymbol)
        {
            return Rectangle(height, width, alternateSymbol, Color);
        }

        public string Rectangle(int height, int width, ConsoleColor alternateColor)
        {
            return Rectangle(height, width, Symbol, alternateColor);
        }

        public string Rectangle(int height, int width, char alternateSymbol, ConsoleColor alternateColor)
        {
            Color = alternateColor;
            string output = "";
            for (int i = 1; i <= height; i++)
            {
                for (int j = 1; j <= width; j++)
                {
                    output += alternateSymbol;
                }
                output += "\n";
            }


            return output;
        }

        public string Triangle(int height)
        {
            return Triangle(height, symbol);
        }

        public string Triangle(int height, char alternateSymbol)
        {
            return Triangle(height, alternateSymbol, Color);
        }
        
        public string Triangle(int heigth, ConsoleColor alternateColor)
        {
            return Triangle(heigth, Symbol, alternateColor);
        }

        public string Triangle(int height, char alternateSymbol, ConsoleColor alternateColor)
        {
            Color=alternateColor;
            string output = "";
            for (int i = 1; i <= height; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    output += alternateSymbol;
                }
                output += "\n";
            }
            return output;

        }

    }
}
