﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Serialization;


namespace OOP
{
    class Overerving
    {
        public static void StartSubmenu()
        {
            //nodig om euro symbool te krijgen
            Console.OutputEncoding = System.Text.Encoding.UTF8;


            Console.Clear();
            Console.WriteLine("********************************");
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("********************************");
            Console.WriteLine("1. Werkstudenten toevoegen (een bestaande klasse uitbreiden via overerving)");
            Console.WriteLine("2. Takenlijst: gewone en terugkerende taken (klassen met aangepaste constructor)");
            Console.WriteLine("3. Ziekenhuis (methodes overschrijfbaar maken)");

            Console.Write("\rKeuze: ");

            int choice = int.Parse(Console.ReadLine());


            switch (choice)
            {
                case 1:
                    Console.Clear();
                    DemonstrateWorkingStudent();
                    break;
                case 2:
                    Console.Clear();
                    DemonstrateTask();
                    break;
                case 3:
                    Console.Clear();
                    DemonstratePatients();
                    break;

                default:
                    Console.Clear();
                    Console.WriteLine("Ongeldige keuze.");
                    break;
            }
        }

        /// <summary>
        /// oefening 1 
        /// </summary>
        public static void DemonstrateWorkingStudent()
        {
            var studentList = new List<StudentH11>();
            studentList.Add(new StudentH11()
            {
                Name = "Babs Janssens",
                Age = 20,
                ClassGroup = ClassGroups.EA1,
                MarkCommunication = 14,
                MarkProgrammingPrinciples = 16,
                MarkWebTech = 19
            });
            studentList.Add(new WorkingStudent()
            {
                Name = "Kyan Janssens",
                Age = 18,
                ClassGroup = ClassGroups.EA1,
                MarkCommunication = 13,
                MarkProgrammingPrinciples = 12,
                MarkWebTech = 17,
                WorkHours = 15,

            });

            foreach (StudentH11 student in studentList)
            {
                student.ShowOverviewStudent();

            }
        }

        /// <summary>
        /// oefening 2
        /// </summary>
        private static void DemonstrateTask()
        {
            ArrayList taskList = new ArrayList();
            string choice = string.Empty;

            while (choice != "4")
            {
                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("1. Een taak maken");
                Console.WriteLine("2. Een terugkerende taak maken");
                Console.WriteLine("3. Taken afdukken");
                Console.WriteLine("4. Einde");
                choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        Console.Clear();
                        Console.WriteLine("Beschrijving van de taak?");
                        string description = Console.ReadLine();
                        taskList.Add(new Task(description));
                        break;
                    case "2":
                        Console.Clear();
                        Console.WriteLine("Beschrijving van de taak?");
                        description = Console.ReadLine();
                        Console.WriteLine("Aantal dagen tussen herhaling?");
                        byte days = byte.Parse(Console.ReadLine());
                        taskList.Add(new RecurringTask(description, days));
                        break;
                    case "3":
                        Console.Clear();
                        foreach (var task in taskList)
                        {
                            Console.WriteLine(task.ToString());
                        }
                        break;
                    case "4":
                        break;
                    default:
                        break;
                }

            }

        }
        private static void DemonstratePatients()
        {
         

            var patient = new Patient("Tim", 12);
            var patientInsured = new InsuredPatient("Vincent", 12);

            patient.ShowCost();
            patientInsured.ShowCost();
        }






    }
}