﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace OOP
{
    class Ticks2000Program
    {

        public static void Main()
        {

            CultureInfo belgiumCI = new CultureInfo("nl-BE");

            DateTime now = DateTime.Now;
            DateTime startDate = new DateTime(2000, 01, 01);

            TimeSpan difference = now - startDate;

            Console.WriteLine($"Sinds {startDate.ToString("dd MMMM yyyy",belgiumCI)} zijn er {difference.Ticks} ticks voorbijgegaan.");

        }
    }
}
