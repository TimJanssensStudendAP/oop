﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace OOP
{
    class RecurringTask : Task
    {
        public byte Days { get; set; }
        public RecurringTask(string description, byte days): base(description)
        {
            this.Days = days;
            Console.WriteLine($"Deze taak moet om de {this.Days} herhaald worden.");
        }

        public override string ToString()
        {
            return $"{base.ToString()} om de {this.Days} dagen";
        }


    }
}
