﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.PatientProgram
{
    class Patient
    {

		private string name;
		private string gender;
		private string lifestyle;
		private int day;
		private int month;
		private int year;

		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public string Gender
		{
			get { return gender; }
			set { gender = value; }
		}
		public string Lifestyle
		{
			get { return lifestyle; }
			set { lifestyle = value; }
		}
		public int Day
		{
			get { return day; }
			set { day = value; }
		}
		public int Month
		{
			get { return month; }
			set { month = value; }
		}
		public int Year
		{
			get { return year; }
			set { year = value; }
		}
		
		public void AddPatient()
		{
			Name = Console.ReadLine();
			Gender = Console.ReadLine();
			Lifestyle = Console.ReadLine();
			Day = int.Parse(Console.ReadLine());
			Month = int.Parse(Console.ReadLine());
			Year = int.Parse(Console.ReadLine());
			
		}

		public string PrintPatient()
		{
		
			return $"{this.Name} ({this.Gender}, {this.Lifestyle})\n" +
				$"geboren {this.Day}-{this.Month}{this.year}";
			
		}




	}
}
