﻿using System;
using OOP.Geometry;
using OOP.Carexe;
using OOP.PatientProgram;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1 Vormen tekenen (h8-vormen)");
            Console.WriteLine("2 Auto's laten rijden (h8-autos)");
            Console.WriteLine("3 Patienten tonen (h8-patienten)");
            Console.WriteLine("4 Honden laten blaffen (h8-blaffende-honden)");

            int choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:

                    ShapesBuilder builder = new ShapesBuilder();

                    Console.WriteLine(builder.Line(10));
                    Console.Write(builder.Rectangle(10, 5));
                    Console.Write(builder.Rectangle(10, 10, '+'));
                    Console.Write(builder.Rectangle(10, 5, ConsoleColor.Green));
                    Console.Write(builder.Rectangle(15, 10, 'x', ConsoleColor.DarkBlue));
                    Console.Write(builder.Triangle(10));
                    Console.Write(builder.Triangle(10, 'Q'));
                    Console.Write(builder.Triangle(10, ConsoleColor.Red));
                    Console.Write(builder.Triangle(5, 'o', ConsoleColor.Cyan));

                    break;
                case 2:
                    Car car1 = new Car();
                    Car car2 = new Car();

                    car1.Gas(5);
                    car1.Brake(3);

                    car2.Gas(10);
                    car2.Brake(1);

                    Console.WriteLine($"Auto 1:{car1.Speed} km/u , afgelegde weg  is {car1.Odometer:F2} km.");
                    
                    Console.WriteLine($"Auto 1:{car2.Speed} km/u , afgelegde weg  is {car2.Odometer:F2} km.");




                    break;
                case 3:

                    Console.WriteLine("Hoe veel patiënten zijn er?"); // aanmaken van de nodige variabelen
                    int numberOfPatients = int.Parse(Console.ReadLine());

                    Patient[] Patients = new Patient[numberOfPatients];


                  

                   //patienten aanmaken en in een array plaatsen
                    for (int i = 0; i < numberOfPatients; i++)
                    {
                        Patient patient = new Patient();
                        patient.AddPatient();
                        Patients[i] = patient;
                    }


                    //door alle patienten in de array Patients lopen en deze afdrukken
                    foreach (var item in Patients)
                    {
                        Console.WriteLine($"{ item.PrintPatient()}");
                    }
                        


                        break;
                case 4:

                    break;
                default:
                    Console.WriteLine("Ongeldig getal!");
                    break;
            }






            Console.ReadKey();
        }


    }
}
