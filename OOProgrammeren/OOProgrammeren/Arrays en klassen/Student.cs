﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class StudentH11
    {
        public string Name { get; set; } = "Onbekend";
        private byte age = 18;

        public byte Age
        {
            get { return age; }
            set
            {
                if (value <= 120)
                {
                    age = value;
                }
            }
        }
        public ClassGroups ClassGroup { get; set; } = ClassGroups.EA1;
        private byte markCommunication = 10;

        public byte MarkCommunication
        {
            get { return markCommunication; }
            set
            {
                if (value <= 20)
                {
                    markCommunication = value;
                }
            }
        }

        private byte markProgrammingPrinciples = 10;

        public byte MarkProgrammingPrinciples
        {
            get { return markProgrammingPrinciples; }
            set
            {
                if (value <= 20)
                {
                    markProgrammingPrinciples = value;
                }
            }
        }

        private byte markWebTech = 10;

        public byte MarkWebTech
        {
            get { return markWebTech; }
            set
            {
                if (value <= 20)
                {
                    markWebTech = value;
                }
            }
        }
        private double overallMark;

        public double OverallMark
        {
            get { return (MarkCommunication + MarkProgrammingPrinciples + MarkWebTech) / 3; }
        }

        public StudentH11()
        {

        }

        public virtual void ShowOverviewStudent()
        {
            
                Console.WriteLine();
                Console.WriteLine($"{this.Name}, {this.Age} jaar");
                Console.WriteLine($"Klas: {this.ClassGroup}");
                Console.WriteLine($"Cijferrapport:");
                Console.WriteLine($"**************");
                Console.WriteLine($"Communicatie \t\t {this.MarkCommunication}");
                Console.WriteLine($"Programming Principles \t {this.MarkProgrammingPrinciples}");
                Console.WriteLine($"Web Technology \t\t {this.MarkWebTech}");
                Console.WriteLine($"Gemiddelde: \t\t {this.OverallMark}");
            
        }



    }
}
