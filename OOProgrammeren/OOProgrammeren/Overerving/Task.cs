﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace OOP
{
    class Task
    {
        public string Description { get; set; }
   
        public Task(string description)
        {
            this.Description = description;
            Console.WriteLine($"Taak {this.Description} is aangemaakt.");
        }
        public override string ToString()
        {
            return $"Taaknaam: {this.Description}";
        }
    }
}
