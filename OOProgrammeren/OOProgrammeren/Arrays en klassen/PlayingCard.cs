﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{

    class PlayingCard
    {
        private int number;
        static Random random = new Random();
        public int Number
        {
            get { return number; }
            set
            {
                if (value > 0 && value <= 13)
                {
                    number = value;
                }
            }
        }
        public Suites Suite { get; set; }

        public PlayingCard(int number, Suites suite)
        {
            this.Number = number;
            this.Suite = suite;
        }

        public static void main()
        {
            List<PlayingCard> cardDeck = GenerateDeck();


            ShowMenu(cardDeck);





            Console.ReadKey();

        }

        public static void ShowMenu(List<PlayingCard> cardDeck)
        {
            string choice = string.Empty;

            while (choice != "5")
            {

                Console.WriteLine("**********");
                Console.WriteLine("Card Games");
                Console.WriteLine("**********");
                Console.WriteLine("1. Haal 1 kaart uit de carddek en toen de overgebleven kaarten");
                Console.WriteLine("2. Speel een spelletje hoger lager");
                Console.WriteLine("5. Einde");

                choice = Console.ReadLine();


                switch (choice)
                {
                    case "1":
                        ShowShuffledDeck(cardDeck);
                        break;
                    case "2":
                        PlayHigherLower(cardDeck);
                        break;
                    default:
                        break;
                }

            }


        }


        /// <summary>
        /// een lijst van alle kaarten maken en deze teruggeven
        /// </summary>
        /// <returns></returns>
        public static List<PlayingCard> GenerateDeck()
        {
            List<PlayingCard> cardDeck = new List<PlayingCard>();

            foreach (Suites suit in Enum.GetValues(typeof(Suites)))
            {
                for (int i = 1; i <= 13; i++)
                {
                    cardDeck.Add(new PlayingCard(i, suit));
                }
            }

            return cardDeck;
        }

        /// <summary>
        /// een kaart uit de lijst halen, deze tonen en daarna alle overgebleven kaarten tonen
        /// </summary>
        /// <param name="cards">lijst van alle "overgebleven" kaarten </param>
        public static void ShowShuffledDeck(List<PlayingCard> cards)
        {

            //random instellen van index 0 tot het aantal kaarten (-1) om deze dan te kunnen deleten
            int indexToDelete = random.Next(0, cards.Count - 1);

            Console.WriteLine($"De kaart die zal verwijderd worden is \"{cards[indexToDelete].Suite} - {cards[indexToDelete].Number}\" ");
            cards.RemoveAt(indexToDelete);

            foreach (PlayingCard item in cards)
            {
                Console.WriteLine($"{item.Suite} {item.Number}");
            }

            Console.WriteLine("Alle kaarten zijn getoond");
        }


        public static void PlayHigherLower(List<PlayingCard> cardDeck)
        {
            Console.Clear();
            Console.WriteLine("Klaar om te spelen");

            int indexPreviousCard = random.Next(0, cardDeck.Count - 1);

            bool keepPlaying = true;

            while (keepPlaying && cardDeck.Count > 0)
            {


                PlayingCard previousCard = cardDeck[indexPreviousCard];
                int numberPreviousCard = previousCard.Number;
                Console.WriteLine($"De vorige kaart had waarde {numberPreviousCard}");
                cardDeck.RemoveAt(indexPreviousCard);

                int indexCurrentCard = random.Next(0, cardDeck.Count - 1);
                PlayingCard currentCard = cardDeck[indexCurrentCard];
                int numberCurrentCard = currentCard.Number;
                cardDeck.RemoveAt(indexCurrentCard);

                Console.WriteLine("Hoger (0), lager (1), gelijk (2)");

                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "0":
                        if (numberCurrentCard > numberPreviousCard)
                            indexPreviousCard = indexCurrentCard;
                        else
                        {
                            Console.WriteLine($"Fout! De waarde was {numberCurrentCard}");
                            keepPlaying = false;
                        }
                        break;
                    case "1":
                        if (numberCurrentCard < numberPreviousCard)
                            indexPreviousCard = indexCurrentCard;
                        else
                        {
                            Console.WriteLine($"Fout! De waarde was {numberCurrentCard}");
                            keepPlaying = false;
                        }
                        break;
                    case "2":
                        if (numberCurrentCard == numberPreviousCard)
                            indexPreviousCard = indexCurrentCard;
                        else
                        {
                            Console.WriteLine($"Fout! De waarde was {numberCurrentCard}");
                            keepPlaying = false;
                        }
                        break;


                    default:
                        break;
                }


            }




            Console.ReadKey();


        }


    }
}
