﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Prijzen
    {

        public static void AskForPrices()
        {
            int repeats = 20;
            Console.WriteLine($"Gelieve {repeats} prijzen in te geven.");
            double[] prices = new double[repeats];
            int count = 0;
            double totalPrice = 0.00;

            while (count < repeats)
            {
                Console.Write("> ");
                double num = double.Parse(Console.ReadLine());
                prices[count] = num;
                totalPrice += num;
                count++;
            }

            foreach (double num in prices)
            {
                if (num > 5)
                {
                Console.WriteLine(num.ToString("0.00"));
                }
            }

            double average = totalPrice / repeats;

            Console.WriteLine($"het gemiddelde bedrag is {average.ToString("0.00")}.");

            Console.ReadKey();
        }


    }
}
