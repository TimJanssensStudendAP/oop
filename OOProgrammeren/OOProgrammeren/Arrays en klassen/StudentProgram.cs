﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class StudentProgram
    {
        public static void Main()
        {
            List<StudentH11> studentH11List = new List<StudentH11>();

            ExecuteStudentMenu(studentH11List);


            Console.ReadKey();
        }


        public static void ExecuteStudentMenu(List<StudentH11> studentH11List)
        {

            Console.Clear();

            int choice;

            do
            {
                Console.WriteLine("***************");
                Console.WriteLine("Wat wil u doen?");
                Console.WriteLine("***************");
                Console.WriteLine("1. Gegevens van studenten tonen");
                Console.WriteLine("2. Student toevoegen");
                Console.WriteLine("3. Gegevens student aanpassen");
                Console.WriteLine("4. Een student uit het systeem verwijderen");
                Console.WriteLine("5. Toon gemiddelde communicatie");
                Console.WriteLine("6. Toon gemiddelde programmeren");
                Console.WriteLine("7. Toon gemiddelde webtechnologie");
                Console.WriteLine("8. Einde");

                choice = int.Parse(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        ShowOverview(studentH11List);
                        break;
                    case 2:
                        Console.Clear();
                        AddStudent(studentH11List);
                        break;
                    case 3:
                        Console.Clear();
                        AddjustStudent(studentH11List);
                        break;
                    case 4:
                        Console.Clear();
                        RemoveStudent(studentH11List);
                        break;
                    case 5:
                        Console.Clear();
                        ShowAverageCommunication(studentH11List);
                        break;
                    case 6:
                        Console.Clear();
                        ShowAverageProgramming(studentH11List);
                        break;
                    case 7:
                        Console.Clear();
                        ShowAverageWebtech(studentH11List);
                        break;
                    case 8:
                        Console.Clear();
                        Console.WriteLine("Einde programma");
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Ongeldige keuze.");
                        break;
                }

            } while (choice != 8);

        }

        public static void AddStudent(List<StudentH11> studentH11List)
        {
            studentH11List.Add(new StudentH11());
        }
        public static void AddjustStudent(List<StudentH11> studentH11List)
        {

            if (studentH11List.Count != 0)
            {
                Console.WriteLine("Overzicht studenten:");
                ShowIndexNumbersAndNames(studentH11List);
                Console.WriteLine("Wat is de indexpositie van de student die je wil aanpassen?");
                int indexStudent = int.Parse(Console.ReadLine());

                Console.WriteLine("********************");
                Console.WriteLine("Wat wil u aanpassen?");
                Console.WriteLine("********************");
                Console.WriteLine("1. Naam");
                Console.WriteLine("2. Leeftijd");
                Console.WriteLine("3. Klasgroep");
                Console.WriteLine("4. Cijfer Communicatie");
                Console.WriteLine("5. Cijfer Programmeren");
                Console.WriteLine("6. Cijfer Webtechnologie");
                int choice = int.Parse(Console.ReadLine());

                Console.WriteLine("Wat is de nieuwe waarde");
                string newValue = Console.ReadLine();

                switch (choice)
                {
                    case 1:
                        studentH11List[indexStudent].Name = newValue;
                        break;
                    case 2:
                        byte age = byte.Parse(newValue);
                        studentH11List[indexStudent].Age = age;
                        break;
                    case 3:
                        ClassGroups clas = (ClassGroups)Enum.Parse(typeof(ClassGroups), newValue);
                        studentH11List[indexStudent].ClassGroup = clas;
                        break;
                    case 4:
                        byte scoreCom = byte.Parse(newValue);
                        studentH11List[indexStudent].MarkCommunication = scoreCom;
                        break;
                    case 5:
                        byte scoreProg = byte.Parse(newValue);
                        studentH11List[indexStudent].MarkProgrammingPrinciples = scoreProg;
                        break;
                    case 6:
                        byte scoreWeb = byte.Parse(newValue);
                        studentH11List[indexStudent].MarkWebTech = scoreWeb;
                        break;

                    default:
                        Console.Clear();
                        Console.WriteLine("Ongeldige keuze.");
                        break;
                }
            }
            else
            {
                Console.WriteLine("Geen studenten in lijst dus niets aan te passen");
            }

        }

        public static void RemoveStudent(List<StudentH11> studentH11List)
        {
            Console.WriteLine("Overzicht studenten:");
            ShowIndexNumbersAndNames(studentH11List);
            Console.WriteLine("Wat is de indexpositie van de te verwijderen student");
            int indexTeVerwijderenStudent = int.Parse(Console.ReadLine());

            studentH11List.RemoveAt(indexTeVerwijderenStudent);

        }

        public static void ShowOverview(List<StudentH11> studentH11List)
        {
            int count = 0;
            foreach (StudentH11 item in studentH11List)
            {
                Console.WriteLine();
                Console.WriteLine($"Student op index: {count++}");
                item.ShowOverviewStudent();
            }
        }

        public static void ShowAverageCommunication(List<StudentH11> studentH11List)
        {
            int total = 0;
            foreach (StudentH11 student in studentH11List)
            {
                total += student.MarkCommunication;
            }
            double avgCommunication = (double)total / studentH11List.Count;
            Console.WriteLine($"Gemmidelde cijfer op communicatie: {avgCommunication:F2}") ;
        }

        public static void ShowAverageProgramming(List<StudentH11> studentH11List)
        {
            int total = 0;
            foreach (StudentH11 student in studentH11List)
            {
                total += student.MarkProgrammingPrinciples;
            }
            double avgProgramming = (double)total / studentH11List.Count;
            Console.WriteLine($"Gemmidelde cijfer op communicatie: {avgProgramming:F2}");
        }

        public static void ShowAverageWebtech(List<StudentH11> studentH11List)
        {
            int total = 0;
            foreach (StudentH11 student in studentH11List)
            {
                total += student.MarkWebTech;
            }
            double avgWebtech = (double)total / studentH11List.Count;
            Console.WriteLine($"Gemmidelde cijfer op communicatie: {avgWebtech:F2}");
        }

        public static void ShowIndexNumbersAndNames(List<StudentH11> studentH11List)
        {

            int count = 0;

            foreach (StudentH11 student in studentH11List)
            {
                Console.WriteLine();
                Console.WriteLine($"{student.Name}\t\t{count++}.");
            }
        }


    }
}
