﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace OOP
{
    class WorkingStudent : StudentH11
    {
        private static Random random = new Random();
        private byte workHours = 10;
        public byte WorkHours
        {
            get { return workHours; }
            set
            {
                if (value <= 0)
                    workHours = 0;
                else if (value >= 20)
                    workHours = 20;
                else
                    workHours = value;
            }
        }

        public bool HasWorkToday()
        {
            if (random.Next(0, 2) == 0)
                return true;
            else
                return false;
        }

        public override void ShowOverviewStudent()
        {
            base.ShowOverviewStudent();
            Console.WriteLine($"Statuut: Werkstudent");
            Console.WriteLine($"Aantal werkuren per week: {this.WorkHours}.");
        }

    }
}
